package br.upis;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class Threads_ProdutoConsumidor
 */

@WebServlet ("/produto_consumidor")
public class Threads_ProdutoConsumidor extends HttpServlet {
    private final int tamanhoBuffer = 5;
    private int[] buffer = new int[tamanhoBuffer];
    private int posicaoProdutor = 0;
    private int posicaoConsumidor = 0;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Implementação não sincronizada
        Produtor p1 = new Produtor();
        Consumidor c1 = new Consumidor();
        p1.start();
        c1.start();
        
        // Implementação sincronizada
        ProdutorSync p2 = new ProdutorSync();
        ConsumidorSync c2 = new ConsumidorSync();
        p2.start();
        c2.start();
    }
    
    // Implementação não sincronizada
    class Produtor extends Thread {
        public void run() {
            while (true) {
                if (posicaoProdutor < tamanhoBuffer) {
                    buffer[posicaoProdutor] = posicaoProdutor;
                    posicaoProdutor++;
                    System.out.println("Produtor produziu: " + (posicaoProdutor-1));
                }
            }
        }
    }
    
    class Consumidor extends Thread {
        public void run() {
            while (true) {
                if (posicaoConsumidor < posicaoProdutor) {
                    System.out.println("Consumidor consumiu: " + buffer[posicaoConsumidor]);
                    posicaoConsumidor++;
                }
            }
        }
    }
    
    // Implementação sincronizada
    class ProdutorSync extends Thread {
        public void run() {
            while (true) {
                synchronized (this) {
                    if (posicaoProdutor < tamanhoBuffer) {
                        buffer[posicaoProdutor] = posicaoProdutor;
                        posicaoProdutor++;
                        System.out.println("Produtor produziu: " + (posicaoProdutor-1));
                    }
                }
            }
        }
    }
    
    class ConsumidorSync extends Thread {
        public void run() {
            while (true) {
                synchronized (this) {
                    if (posicaoConsumidor < posicaoProdutor) {
                        System.out.println("Consumidor consumiu: " + buffer[posicaoConsumidor]);
                        posicaoConsumidor++;
                    }
                }
            }
        }
    }
}
