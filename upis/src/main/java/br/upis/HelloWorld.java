package br.upis;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.* ;


import org.apache.tomcat.jakartaee.commons.compress.utils.IOUtils;

@WebServlet ("/hello-world")
public class HelloWorld extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorld() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @param getHtml 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * O primeiro parametro é de entrada (request) e o segundo é saida (response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//response.getWriter().println("Ola mundo");
		response.getWriter().println(getHtml());
	}
	

	private String getHtml() throws IOException {
        StringBuffer sb = new StringBuffer();
        //File file = new File("C:\\imagem-teste2.jpeg");
        
        //byte[] imageBytes = new byte[file.InputStream.Length + 1];
        //byte[] imageBytes = IOUtils.toByteArray(new URL("C:\\imagem-teste2.jpeg"));
        //String base64 = Base64.getEncoder().encodeToString(imageBytes);
        
        
        String base64="";
        InputStream iSteamReader = new FileInputStream("C:\\imagem-teste2.jpeg");
        byte[] imageBytes = IOUtils.toByteArray(iSteamReader);
        base64 = Base64.getEncoder().encodeToString(imageBytes);
        //System.out.println(base64);
       
        
        sb.append("<html>");
        sb.append("<head>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<strong>" + "Ola, mundo!" + "</strong>");
        sb.append("</br>");
        sb.append("<img src=\"data:image/jpeg;base64," + base64 + "\" style=\"width: auto; height: 200px;\" alt=\" Documento\"/>");
        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
