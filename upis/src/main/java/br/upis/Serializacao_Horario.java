package br.upis;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.io.*;


public class Serializacao_Horario extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Horario horario = new Horario(22, 45, 0);

        // Serialização
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(horario);
        oos.flush();
        byte[] horarioSerializado = bos.toByteArray();
        oos.close();
        bos.close();

        // Desserialização
        ByteArrayInputStream bis = new ByteArrayInputStream(horarioSerializado);
        ObjectInputStream ois = new ObjectInputStream(bis);
        Horario horarioDesserializado = null;
		try {
			horarioDesserializado = (Horario) ois.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ois.close();
        bis.close();

       
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h2>Horario serializado: " + horario + "</h2>");
        out.println("<h2>Horario desserializado: " + horarioDesserializado + "</h2>");
        out.println("</body></html>");
    }
}
